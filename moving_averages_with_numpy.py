import numpy as np
import matplotlib.pyplot as plt
from cryptocompare_api import CryptoCompareAPI


def moving_average_200w(data_set, window):
    """
    Calculate 200 weekly moving average using numpy convolution
    """
    weights = np.ones(window) / window
    return np.convolve(data_set, weights, mode='valid')


def moving_average_200d(data_set, window):
    """
    Calculate 200 daily moving average using numpy convolution
    """
    weights = np.ones(window) / window
    return np.convolve(data_set, weights, mode='valid')


def plot_data(btc_price, mva_200w, mva_200d):
    """
    Plot bitcoin price and moving averages
    """
    time_range = btc_price.keys()
    plt.plot(btc_price, label='BTC 2-daily')
    plt.plot(time_range[len(time_range)-len(mva_200w):], mva_200w, label='200 weekly MA')
    plt.plot(time_range[len(time_range)-len(mva_200d):], mva_200d, label='200 daily MA')
    plt.title("BTC price with 200 weekly and 200 daily moving averages")
    plt.xlabel("Date")
    plt.ylabel("Price (USD)")
    plt.legend()
    plt.grid()
    plt.show()


def main():
    """
    Python 'main' function.
    """
    # Obtain BTC close historic prices
    crypto_compare_api = CryptoCompareAPI("put_api_key_here")
    btc_price = crypto_compare_api.get_historic_price('BTC')

    # Calculate moving averages
    mva_200w = moving_average_200w(btc_price, 1400)
    mva_200d = moving_average_200d(btc_price, 200)

    # Plot BTC price and moving averages
    plot_data(btc_price, mva_200w, mva_200d)


if __name__ == "__main__":
    main()
