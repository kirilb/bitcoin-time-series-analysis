import pandas as pd
import matplotlib.pyplot as plt
from cryptocompare_api import CryptoCompareAPI


def plot_data(df):
    """
    Plot bitcoin price and moving averages
    """
    plt.plot(df)
    plt.title("BTC price with 200 weekly and 200 daily moving averages")
    plt.xlabel("Date")
    plt.ylabel("Price (USD)")
    plt.legend(['btc_price', 'mva_200_weekly', 'mva_200_daily'])
    plt.grid()
    plt.show()


def main():
    """
    Python 'main' function.
    """
    # Obtain BTC close historic prices
    crypto_compare_api = CryptoCompareAPI("put_api_key_here")
    btc_price = crypto_compare_api.get_historic_price('BTC')

    # Calculate moving averages
    mva_200w = btc_price.rolling(window=1400).mean()
    mva_200d = btc_price.rolling(window=200).mean()

    # Convert btc_price pandas series into dataframe and add mva_200w and mva_200d columns to it
    df = pd.DataFrame(btc_price)
    df["mva_200w"] = mva_200w.values
    df["mva_200d"] = mva_200d.values

    # Plot BTC price and moving averages
    plot_data(df)


if __name__ == "__main__":
    main()
