import numpy as np
import matplotlib.pyplot as plt
import copy
from cryptocompare_api import CryptoCompareAPI


def calculate_percentage_diff(prices):
	"""
	Takes prices as pandas series and returns percentage difference between every two adjacent days
	"""
	# Make a copy of prices array
	prices_copy = np.copy(prices)

	# Remove the last element from prices_copy
	prices_copy = prices_copy[:-1]

	# Remove the first element from prices
	prices = prices[1:]

	percentage_diff = ((prices - prices_copy)/prices_copy)
	percentage_diff = np.around(percentage_diff, decimals=3)
	return percentage_diff


def random_walk_matrix(initial_position, std, num_of_walks, num_of_steps):
	"""
	Returns a matrix with each row containing starting price and simulated prices for a random walk.
	Number of rows in a matrix is equal to num_of_walks variable
	"""
	# Initialise a matrix with required size. 1 is added to num_of_Steps to include
	# last known price as a starting point
	a = np.zeros(shape=(num_of_walks, num_of_steps + 1))
	for i in range(num_of_walks):
		prices = list()  #Will contain simulated prices for a walk
		prices.append(initial_position)  #Last known price as a starting point
		for j in range(num_of_steps):
			current_diff = np.random.normal(0, std)
			prices.append((prices[j]) * (1 + current_diff))

		#To change the prices list without changing the previous reference
		a[i] = copy.copy(prices)

	return a

def prediction_statistics(random_walks_matrix):
	"""
	Calculates 95% confidence interval upper and lower bounds of the last prediction step across all random walks
	"""
	last_column = random_walks_matrix[-1]
	mean = last_column.mean()
	std = last_column.std()
	upper_bound = round((mean + 2 * std), 2)
	lower_bound = round((mean - 2 * std),2)
	print("95% confidence interval upper bound is {} and lower bound is {}".format(upper_bound, lower_bound))

def main():
	# Define number of walks and steps to simulate
	num_of_walks = 10000
	num_of_steps = 30

	# Obtain BTC close historic prices
	crypto_compare_api = CryptoCompareAPI("put_api_key_here")
	prices = crypto_compare_api.get_historic_price('BTC')

	# Calculate percentage price difference between two adjacent days
	percentage_diff = calculate_percentage_diff(prices)

	# Calculate standard deviation
	std = percentage_diff.std()

	a = random_walk_matrix(prices[-1], std, num_of_walks, num_of_steps)

	# Calculate and print upper and lower bounds of the last matrix column
	prediction_statistics(a)

	# Each walk simulated prices are stored as matrix row.
	# They have to be transposed into columns in order to be plotted
	plt.plot(a.T)
	plt.title("Random Walk Future Price Simulation")
	plt.xlabel("Future Day")
	plt.ylabel("Price (USD)")
	plt.show()

if __name__ == "__main__":
	main()


