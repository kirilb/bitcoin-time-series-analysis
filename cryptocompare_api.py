import requests
import json
import pandas as pd
import time
import sys


class CryptoCompareAPI(object):
	"""
	This class includes methods to interact with cryptocompare API.
	More details about API can be found at: https://min-api.cryptocompare.com/documentation
	"""

	def __init__(self, api_key):
		self.api_key = api_key

	@staticmethod
	def make_rest_call(url):
		"""
		Takes URL as a parameter, makes a REST call and
			returns data converted to json format
		"""
		http_response = requests.get(url)
		if http_response.status_code == 200:  # success
			data = json.loads(http_response.text)
		else:
			print('Status code:', http_response.status_code, 'Problem with the request. Exiting.')
			sys.exit()
		return data

	def get_historic_price(self, currency):
		"""
		Returns historical daily data before current timestamp
		Keeps requesting batches using: &limit=100&toTs={the earliest timestamp received}
			until the 'close' price is zero. The max possible 'limit' parameter is 2000.
		"""
		current_epoch = str(round(time.time()))
		total_df = pd.DataFrame()
		while True:
			url = "https://min-api.cryptocompare.com/data/histoday?fsym=" + currency + "&tsym=USD&limit=2000&toTs=" \
				+ current_epoch
			data = self.make_rest_call(url)
			# 2000 values as returned by rest call are stored in temp_df dataframe
			temp_df = pd.DataFrame(data['Data'])
			# values from all rest calls are concatenated in total_df dataframe
			total_df = pd.concat([total_df, temp_df])
			# If the first temp_df dataframe row close price value is more than zero then
			# more historic values exist and another rest call will be made with updated
			# current_epoch
			if temp_df['close'][0] > 0:
				current_epoch = str(temp_df['time'][0])
			else:
				total_df.time = pd.to_datetime(total_df['time'], unit='s')
				total_df = total_df.set_index(total_df.time)
				total_df = total_df.sort_index()
				total_df = total_df.close
				break
		# Remove any zero values from concatenated dataframe and return
		return total_df.where(total_df > 0).dropna()