import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from cryptocompare_api import CryptoCompareAPI


def r_squared(btc_price_log, btc_price_fit):
    """
    Computes R squared value of the log fit.
    Prints the result to STDOUT
    """
    residuals = btc_price_log - btc_price_fit.T
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((btc_price_log - np.mean(btc_price_log)) ** 2)
    result = round((1 - (ss_res / ss_tot)), 2)
    return result

def plot_data(df, r):
    """
    Plot bitcoin price log curve, lower and upper bounds
    """
    plt.plot(df)
    plt.legend(['BTC close price log daily','BTC price log curve fit', '- 2sigma', '+ 2sigma'])
    plt.title("BTC price growth logarithmic trend (log base 10)")
    plt.xlabel("Date")
    plt.ylabel("Price change")
    plt.grid()
    plt.figtext(0.5, 0.03, "R_squared value: {}".format(r), wrap=True,
                horizontalalignment='center', fontsize=12, bbox={'facecolor':'grey', 'alpha':0.5, 'pad':10})
    plt.show()


def main():
    """
    Python 'main' function.
    """
    # Obtain BTC close historic prices
    crypto_compare_api = CryptoCompareAPI("put_api_key_here")
    btc_price = crypto_compare_api.get_historic_price('BTC')

    # Convert BTC price to log10 format
    btc_price_log = np.log10(btc_price)

    # Fit logarithmic curve using least squares method
    x = np.arange(1, len(btc_price)+1) # make sure x starts from 1 as log0 does not exist
    best_vals, covar = curve_fit(lambda t, a, b: a+b*np.log10(t), x, btc_price_log)

    # Log fitted line values
    btc_price_fit = best_vals[0] + best_vals[1]*np.log10(x)

    # Calculate lower and upper error bands
    standard_dev = np.sqrt(np.diag(covar))

    lower_error = (best_vals[0] - 2*standard_dev[0]) + (best_vals[1] - 2*standard_dev[1])*np.log10(x)
    upper_error = (best_vals[0] + 2*standard_dev[0]) + (best_vals[1] + 2*standard_dev[1])*np.log10(x)

    # Convert btc log price pandas series into dataframe and add lower_error and upper_error columns to it
    df = pd.DataFrame(btc_price_log)
    df['btc_price_fit'] = btc_price_fit.T
    df['lower_error'] = lower_error.T
    df['upper_error'] = upper_error.T

    # Calculate R squared value of the fit
    r = r_squared(btc_price_log, btc_price_fit)

    # Plot BTC price, log fit and error bounds
    plot_data(df, r)


if __name__ == "__main__":
    main()
